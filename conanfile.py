import os

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import copy, get
from conan.tools.layout import basic_layout

required_conan_version = ">=1.52.0"


class CppDocConan(ConanFile):
    name = "cppdoc"
    description = "CMake Module for an easy generation of C++ documentation"
    topics = ("conan", "cmake", "sphinx", "doxygen", "breathe", "exhale")
    homepage = "https://gitlab.com/bproto/cppdoc"
    license = ("Apache 2.0",)

    url = "https://gitlab.com/bproto/conan-cppdoc"

    settings = "arch", "compiler", "os", "build_type"
    no_copy_source = False

    def layout(self):
        basic_layout(self, src_folder="src")

    def source(self):
        get(
            self,
            **self.conan_data["sources"][self.version],
            destination=self.source_folder,
            strip_root=True
        )

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(
            self,
            "LICENSE",
            src=self.source_folder,
            dst=os.path.join(self.package_folder, "licenses"),
        )

        cmake = CMake(self)
        cmake.install()

    def package_id(self):
        self.info.clear()

    def package_info(self):
        self.cpp_info.builddirs = ["res/cppdoc/cmake", "res/cppdoc/lib"]
        self.cpp_info.resdirs = ["res"]

        cmake_file = os.path.join("cppdoc", "lib", "Index.cmake")
        self.cpp_info.set_property("cmake_build_modules", [cmake_file])

        # TODO(szinchen): Remove this when migration to Conan 2 is completed.
        for generator in ["cmake_find_package", "cmake_find_package_multi"]:
            self.cpp_info.build_modules[generator].append(cmake_file)
