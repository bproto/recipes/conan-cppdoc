/*
 * Copyright 2022-2023 Zinchenko Serhii <zinchenko.serhii@pm.me>
 *
 * CppDoc: Example project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __CPPDOC_EXAMPLE_MODULEA_ACCESSOR_HPP__
#define __CPPDOC_EXAMPLE_MODULEA_ACCESSOR_HPP__

#include <memory>

namespace Example {
namespace ModuleA {

/**
 * @brief Main entry point for the "A" module.
 *
 * @par Thread Safety
 *   - @e Distinct @e objects: Safe.
 *   - @e Shared @e objects: Safe.
 *
 * @warning This interface @b MUST @b NOT be implemented by a user. This
 *     interface is intended to be used as is by the internals of specific
 *     protocol implementation.
 */
class Accessor
{
public:

    /**
     * @brief Default virtual destructor.
     */
    virtual ~Accessor() = default;

    /**
     * @brief Creates a new accessor object.
     *
     * @return A pointer to a new accessor object if it was created, @c nullptr
     *     otherwise.
     */
    [[nodiscard]]
    static std::unique_ptr<Accessor> create();
};

} // namespace ModuleA
} // namespace Example

#endif // __CPPDOC_EXAMPLE_MODULEA_ACCESSOR_HPP__
