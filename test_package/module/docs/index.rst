Module "A"
==========

An example of C++ module with documentation.
See more in :doc:`developer notes <developer_notes>`.

.. toctree::
   :hidden:
   :maxdepth: 1

   developer_notes.rst

.. include:: unabridged_api.rst
