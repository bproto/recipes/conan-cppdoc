# conan-cppdoc

[Conan.io](https://conan.io) package for [CppDoc](https://gitlab.com/cpptp/cppdoc)

## Building the package

Install the Conan package manager and the Conan package tools with `pip`.

```bash
$ pip install conan==1.59.0
```

Then simply run this Conan command in the root of the repository to build `cppdoc` package.

```bash
$ conan create . --name=cppdoc --version=<version> --build-require
```

Where `<version>` string can be found in `conandata.yml` file in the root of this repository.

If you don't want to run tests, you can add `--test-folder=` to the end of the above command.

## Using Conan remotes

If you don't want to clone this repository each time and then do a manual installation, you can add `b110011/bproto` remote and have more or less reproducible builds.

```
$ conan remote add bproto "https://gitlab.com/api/v4/projects/40501309/packages/conan"
```

Other variant can be uploading this recipe to your own [artifactory][jfrog-ara] instance or anything similar to it.

[jfrog-ara]: https://jfrog.com/artifactory/

## Using the package

Conan integrates with different build systems.
You can declare which build system you want your project to use setting in the **[generators]** section of the [conanfile.txt][conanfile-txt] or using the generators attribute in the [conanfile.py][conanfile-py].
Here, there is some basic information you can use to integrate `cppdoc` in your own project.
For more detailed information, please check the Conan documentation.

[conanfile-py]: https://docs.conan.io/en/latest/reference/conanfile/attributes.html#generators
[conanfile-txt]: https://docs.conan.io/en/latest/reference/conanfile_txt.html#generators

### Using cppdoc with CMake

[Conan CMake generators](https://docs.conan.io/en/latest/reference/conanfile/tools/cmake.html)

* [CMakeDeps][cmakedeps]: generates information about where the `cppdoc` library and its dependencies are installed together with other information like version, flags, and directory data or configuration.
  CMake will use this files when you invoke `find_package()` in your *CMakeLists.txt*.

* [CMakeToolchain][cmaketoolchain]: generates a CMake toolchain file that you can later invoke with CMake in the command line using `-DCMAKE_TOOLCHAIN_FILE=conantoolchain.cmake`.

[cmakedeps]: https://docs.conan.io/en/latest/reference/conanfile/tools/cmake/cmakedeps.html
[cmaketoolchain]: https://docs.conan.io/en/latest/reference/conanfile/tools/cmake/cmaketoolchain.html

Declare these generators in your `conanfile.txt` along with your `cppdoc` dependency like:

```ini
[requires]
cppdoc/latests@b110011/bproto

[generators]
CMakeDeps
CMakeToolchain
```

To use `cppdoc` in a simple CMake project with this structure:

```
.
|-- CMakeLists.txt
|-- conanfile.txt
`-- docs
    `-- index.rst
`-- include
    `-- ..hpp
`-- src
    `-- ..cpp
    `-- ..hpp
```

Your `CMakeLists.txt` could look similar to this:

```cmake
cmake_minimum_required(VERSION 3.15)

project(cppdoc_package CXX)

add_library("${PROJECT_NAME}" INTERFACE)

# Setup documentation

set(CPPDOC_BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}")
set(CPPDOC_SOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}")

find_package(cppdoc REQUIRED NO_MODULE)

# Add documentation

cppdoc_add_cpp_files_dir("${PROJECT_NAME}"
    SOURCES
        "${CMAKE_CURRENT_SOURCE_DIR}/include"
)
cppdoc_add_rst_files_dir("${PROJECT_NAME}" "${CMAKE_CURRENT_SOURCE_DIR}/docs")

# Documentation

cppdoc_generate_docs(ALL)
```

To install `cppdoc`, its dependencies and build your project, you just have to do:

```bash
# for Linux/macOS
$ conan install . --install-folder cmake-build-release --build=missing
$ cmake . -DCMAKE_TOOLCHAIN_FILE=cmake-build-release/conan_toolchain.cmake -DCMAKE_BUILD_TYPE=Release
$ cmake --build .

# for Windows and Visual Studio 2017
$ conan install . --output-folder cmake-build --build=missing
$ cmake . -G "Visual Studio 15 2017" -DCMAKE_TOOLCHAIN_FILE=cmake-build/conan_toolchain.cmake
$ cmake --build . --config Release
```